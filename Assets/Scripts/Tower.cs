﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{

    GameObject mobs;

    public GameObject other_tower;

    public GameObject Unit;

    public int UnitsCount = 10;
    public float SpawnInterval = 1f;


    IEnumerator iSendUnits()
    {
        while (UnitsCount > 0)
        {

            GameObject go = Instantiate(Unit, transform.position, Quaternion.identity, mobs.transform);

            Unit u = go.GetComponent<Unit>();
            u.SetDestination(other_tower);

            UnitsCount--;

            yield return new WaitForSeconds(SpawnInterval);
        }
    }


    void SendUnits()
    {
        StartCoroutine(iSendUnits());
    }


    // Use this for initialization
    void Start()
    {
        mobs = GameObject.FindWithTag("mobs");

        SendUnits();
    }

}
