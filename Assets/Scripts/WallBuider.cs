﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallBuider : MonoBehaviour
{

    public GameObject rock;
    public int WallBlockCount;


    GameObject walls;


    void BuildWall()
    {
        float min_scale = 1f;
        float max_scale = 3f;


        Vector3 start_pos = transform.position;
        Vector3 direction = transform.rotation * Vector3.forward;


        for (int count = 0; count < WallBlockCount; count++)
        {
            Vector3 pos = count * direction + start_pos;
            Quaternion rot = Quaternion.Euler(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f));

            GameObject new_rock = Instantiate(rock, pos, rot, walls.transform);
            new_rock.transform.localScale = new Vector3(Random.Range(min_scale, max_scale), Random.Range(min_scale, max_scale), Random.Range(min_scale, max_scale));

        }
    }


    // Use this for initialization
    void Start()
    {
        walls = GameObject.FindWithTag("walls");
        BuildWall();
        Destroy(gameObject);



    }


}
