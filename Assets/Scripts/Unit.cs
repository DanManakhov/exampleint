﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{

    [HideInInspector]
    public GameObject TargetTower;

    float speed = 5f;
    float spawn_radius = 1f;


    Vector3 GetDir()
    {
        return Vector3.Normalize(TargetTower.transform.position - transform.position);
    }

    void DoStep()
    {
        transform.Translate(GetDir() * speed * Time.deltaTime);

    }


    public void SetDestination(GameObject target)
    {
        TargetTower = target;

        Vector3 rand_spawn_pos = new Vector3(Random.Range(-spawn_radius, spawn_radius), 0, Random.Range(-spawn_radius, spawn_radius));

        transform.Translate(GetDir() * GameState.TowerRadius * 1.2f + rand_spawn_pos);

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("tower"))
            Destroy(gameObject);
    }


    // Update is called once per frame
    void Update()
    {
        DoStep();
    }
}
