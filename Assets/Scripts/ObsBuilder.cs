﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ObsBuilder : MonoBehaviour
{
    public GameObject[] prefabs;

    public float width;
    public float height;

    public int Count;

    GameObject obstacles;

    float min_scale = 1f;
    float max_scale = 3f;



    void PlaceObstacles()
    {

        for (int i = 0; i < Count; i++)
        {
            GameObject temp_prefab = prefabs[Random.Range(0, prefabs.Length)];


            float rand_x = Random.Range(-width / 2, width / 2);
            float rand_y = Random.Range(-height / 2, height / 2);

            Vector3 pos = new Vector3(rand_x, 0, rand_y);


            Quaternion rot = Quaternion.Euler(0, Random.Range(0f, 360f), 0);

            GameObject new_rock = Instantiate(temp_prefab, pos, rot, obstacles.transform);
            new_rock.transform.localScale = new Vector3(Random.Range(min_scale, max_scale), Random.Range(min_scale, max_scale), Random.Range(min_scale, max_scale));
        }

    }



    // Use this for initialization
    void Start()
    {
        obstacles = GameObject.FindWithTag("obstacles");
        PlaceObstacles();

        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
